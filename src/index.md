---
layout: cv.hbs
title: Benjamin Wilhelm's CV
---

Benjamin Wilhelm
================

Computer Scientist

<div id="webaddress">
  <a href="https://bwilhelm.net"> https://bwilhelm.net </a>
| <a href="https://github.com/HedgehogCode"> GitHub: HedeghogCode </a>
<span class="no-print">| <a href="/cv_benjamin_wilhelm.pdf"> PDF Version</a></span>
</div>


Interests
---------

Computer Vision and Image Analysis

Deep learning


Work Experience
---------------

**Software Developer, KNIME GmbH** (Konstanz, Germany)
`March 2022 - Present`

- Building a new fast and powerful Python integration based on Apache Arrow and Py4J

**Working Student, KNIME GmbH** (Konstanz, Germany)
`March 2018 - Feb 2022`

- Worked on multiple KNIME integrations, especially the TensorFlow, Deep Learning, Tableau, Power BI, and Python Integration
- Worked on the new columnar table backend based on Apache Arrow
- Programmed mainly in Java and Python
- Contributed to open-source projects
- Other software development tasks

**Intern, Max Plank Institute of Molecular Cell Biology and Genetics** (Dresden, Germany)
`Sep 2017 - Feb 2018`

- Integrate TensorFlow in ImageJ/Fiji
- Integrate TensorFlow in KNIME
- Contribution to the CSBDeep/CARE paper by making it run in Fiji and KNIME
- Minor programming tasks in Java, like a tiled execution for KNIME Image Processing

**Research assistant, University of Konstanz** (Konstanz, Germany)
`Oct 2015 - Aug 2017`

- Programming in Java
- Building a cluster executor for KNIME

Education
---------

**M.S. in Computer and Information Science, University of Konstanz** (Konstanz, Germany)
`Oct 2019 - Feb 2022`

- Focus on computer vision and machine learning
- Image Analysis and Computer Vision I and II
- Seminar about Recent Developments in Deep Learning
- Master's thesis about image reconstruction with a GAN Gaussian denoiser
- Overall grade "very good with distinction"

**B.S. in Computer Science, University of Konstanz** (Konstanz, Germany)
`Oct 2014 - Oct 2019`

- All basic courses
- Mechanics in Physics and Mathematical Logic
- Specialization in Data Mining and Computer Vision
- Ranked 7th in the Freesound General-Purpose Audio Tagging Challenge on Kaggle as part of the Lecture "Advanced Data Challenge"
- Seminar about Energy Minimization Methods in Computer Vision
- Bachelor's thesis about the segmentation of microscope image data
- Overall grade "with distinction"
- Received the VEUK Award ("Preis des Vereins der Ehemaligen der Universität Konstanz (VEUK e.V.)")

**High-school diploma, Erich-Hauser-Gewerbeschule** (Rottweil, Germany)
`Sep 2011 - Jul 2014`

- High-school with focus in Computer Science


Skills
------

Very familiar with
- Java, Python, Git, Linux, Apache Arrow, TensorFlow, Eclipse, Scrum

Familiar with
- MATLAB, C, C++, JavaScript, Bash, LaTeX, Travis CI, Jenkins
