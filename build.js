var Metalsmith = require('metalsmith')
var sass = require('@metalsmith/sass')
var filedata = require('metalsmith-filedata')
var markdown = require('@metalsmith/markdown')
var layouts = require('@metalsmith/layouts')
var html_pdf = require('./lib/html_pdf')
// var debug = require('metalsmith-debug')

Metalsmith(__dirname)
  .metadata({
    site_name: "Benjamin Wilhelm's CV",
    site_description: "This website contains the CV of Benjamin Wilhelm.",
    site_generator: "Metalsmith",
    site_url: "https://cv.bwilhelm.net",
    site_source: "https://gitlab.com/bewilhelm/cv.bwilhelm.net",
    twitter_username: "bewilh",
    github_username: "HedgehogCode",
    email: "benjamin@b-wilhelm.de"
  })
  .source('./src')
  .destination('./public')
  .clean(true)
  .use(sass())
  .use(filedata({
    pattern: 'scss/style.css',
    key: 'style'
  }))
  .use(markdown())
  .use(layouts({
    default: 'cv.hbs',
    pattern: "**/*.html"
  }))
  .use(
    html_pdf({
      input_file: "index.html",
      output_file: "cv_benjamin_wilhelm.pdf",
      pdf_options: {
        format: "A4",
        margin: {
          top: "0.5in",
          bottom: "0.5in",
          left: "0.5in",
          right: "0.5in"
        }
      }
    })
  )
  // .use(debug())
  .build(function (err, files) {
    if (err) { throw err; }
  });
