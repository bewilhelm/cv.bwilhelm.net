# CV of Benjamin Wilhelm

The curriculum vitae of Benjamin Wilhelm as webpage and PDF.

## Getting Started

### Dependencies

Make sure you have `npm` installed.

Install the dependencies:

```
$ npm install
```

### Building

```
$ npm start
```

## Acknowledgments

- __[metalsmith](https://github.com/segmentio/metalsmith)__: Metalsmith for building the html and pdf
- __[markdown-cv](https://github.com/elipapa/markdown-cv)__: Basics for the stylesheet and template
- __[puppeteer](https://github.com/puppeteer/puppeteer/)__: Saving the html page to pdf
- __[Blogpost about html to pdf](https://blog.risingstack.com/pdf-from-html-node-js-puppeteer/)__

## License

The source code (everything but the `cv/index.md` file) is licensed under the [MIT License](http://opensource.org/licenses/mit-license.php).

## TODOs

* Change theme
