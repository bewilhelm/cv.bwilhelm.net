const puppeteer = require('puppeteer')

async function printPDF(html, opts) {
    const browser = await puppeteer.launch({
        headless: true,
        args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
    const page = await browser.newPage();
    await page.setContent(html, { waitUntil: 'networkidle0' })
    const pdf = await page.pdf(opts);

    await browser.close();
    return pdf
}

const plugin = opts => (files, metalsmith, done) => {

    const html = files[opts.input_file].contents
    printPDF(html.toString(), opts.pdf_options).then((buffer) => {
        files[opts.output_file] = {}
        files[opts.output_file].contents = buffer
        setImmediate(done)
    })
};

// Expose `plugin`.
module.exports = plugin
